; Presses space character in random intervals between 1-10 minutes.

Loop
{
    Random, rand, 1 * 1000 * 60, 10 * 1000 * 60
    Sleep rand
    Send {Space}
}