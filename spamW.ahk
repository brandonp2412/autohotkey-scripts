; On holding w, multiplies the amount of w keypresses sent to the current program.
; Press F12 to toggle on and off.

#MaxHotkeysPerInterval 999999999999999999
#Escape::Suspend

$w::
  While GetKeyState("w","P") {
    Send, {w down}
    Sleep 10
    Send, {w up}
    Sleep 10
}

Return

f12::
Suspend,Toggle

return
